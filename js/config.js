/**
 * FLYROBE - Responsive Dashboard
 *
 * FLYROBE theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/login");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('dashboards', {
            abstract: true,
            url: "/dashboards",
            templateUrl: "views/common/content.html?v=17102016-1"
        })
        .state('login', {
            url: "/login",
            templateUrl: "views/login.html?v=20170612-1",
            data: { pageTitle: 'OFS'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe', {
            abstract: true,
            url: "/flyrobe",
            templateUrl: "views/common/content.html?v=17102016-1"
        })
        .state('flyrobe.home', {
            url: "/home",
            templateUrl: "views/home/home.html?v20181207-1",
            data: { pageTitle: 'OFS' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.consignment', {
            url: "/package",
            templateUrl: "views/consignment/consignment.html?v20181207-1",
            data: { pageTitle: 'OFS' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
    ;

}
angular
    .module('flyrobe')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;

        //function to get previous state
        /*$rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            mainVm.previousState = from.name;
        });*/


    });
