/**
 ********** Created by Surbhi Goel ************
 **/

function loginCtrl($state,$timeout,$localStorage,$scope,$http,SweetAlert,$filter,commonService){

    login = this;
    login.account = {};
    login.otp_screen = false;
    login.spinners = false;


    if(!$localStorage.am_flyrobe){
        $localStorage.am_flyrobe = {};
    }

    {
        $localStorage.am_flyrobe.email = "";
    }


    login.loginToDashboard          = loginToDashboard; //function to login
    login.sendOTP                   = sendOTP;
    login.callSendOTPApi            = callSendOTPApi;

    /**********************************************
                function to login
     **********************************************/
    function loginToDashboard(){

        login.spinners = true;

            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + "vendor/otp-login/",
                data: {
                    "refresh_token" : login.refresh_token,
                    "otp" : login.account.otp
                }
            }).success(function (response) {

                login.spinners = false;

                if (response.is_error || response.status == 106) {

                    if (response.status == 106) {
                        login.authMsg = "Please enter valid OTP.";
                    }
                    else{
                        login.authMsg = response.message;
                    }

                    $timeout(function () {
                        login.authMsg = "";
                    },4000);

                    return false;

                }
                else {

                    $localStorage.am_flyrobe = {
                        access_token : response.data.access_token,
                        user_name : response.data.user_name
                    }

                    $state.go('flyrobe.home');
                }


            }).error(function (response) {

            });



    }

    function sendOTP() {

        if(!login.account.phone || login.account.phone.length != 10){

            SweetAlert.swal({
                title: "",
                text: "Please enter valid 10 digit phone number."
            });
            return false;

        }

        login.spinners = true;
        login.callSendOTPApi('send');

    }

    function callSendOTPApi(type) {

        login.authMsg = "";

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall2 + "vendor/send_otp/",
            data: {
                "phone_number": login.account.phone
            }
        }).success(function (response) {

            login.spinners = false;

            if (response.is_error || response.status == 106) {

                if (response.status == 106) {
                    login.authMsg = "Please enter registered number.";
                }
                else{
                    login.authMsg = response.message;
                }
                return false;

            }
            else {

                login.refresh_token = response.data.refresh_token;

                login.authMsg = "OTP has been send to your registered number.";
            }

            $timeout(function () {
                login.authMsg = "";
            },4000);

            if(type == 'send'){
                login.otp_screen = true;
            }


        }).error(function (response) {

        });



    }

}


angular
    .module('flyrobe')
    .controller('loginCtrl', loginCtrl);