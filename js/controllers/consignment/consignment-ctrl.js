/**
 ********** Created by Surbhi Goel ************
 **/

function consignmentCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert, mainServices,DTOptionsBuilder,
                         ngDialog) {

    con = this;

    con.spinners     = true;
    con.show_rhs     = false;

    con.selected_items     = [];


    mainVm.flyrobeState     = "Package";
    $localStorage.am_flyrobe.state = mainVm.flyrobeState;

    con.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withDisplayLength(50)
        .withOption('order', [2, 'asc'])
        .withButtons([
            {extend: 'copy'}
        ]);// dataTable Searching , Sorting & pagination


    con.exportToCsv                 = exportToCsv;
    con.getConsignmentList          = getConsignmentList;
    con.getConsignmentDetails       = getConsignmentDetails;
    con.openModalForImage           = openModalForImage;
    con.selectItem                  = selectItem;
    con.updateStatus                = updateStatus;


    con.getConsignmentList(); //Default Call

    function getConsignmentList() {


        con.spinners     = true;
        con.show_rhs     = false;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'vendor/consignment?access_token=' + $localStorage.am_flyrobe.access_token +
            '&project=id,grand_price,is_active,received_items,received_on,reserve_for_days,sent_items,should_be_return_on,status',
            headers: mainVm.headers
        }).success(function (response) {

            con.spinners     = false;

            if(response.data.length){

                //column def
                {
                    var column = [];
                    con.csv_header = [];

                    var col_name = {
                        name : 'id',
                        displayName: 'ID',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('ID');

                    var col_name = {
                        name : 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Status');

                    var col_name = {
                        name : 'grand_price',
                        displayName: 'Grand Price',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Grand Price');

                    var col_name = {
                        name : 'is_active',
                        displayName: 'Active',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Active');

                    var col_name = {
                        name : 'received_items',
                        displayName: 'Received Items',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Received Items');

                    var col_name = {
                        name : 'received_on',
                        displayName: 'Received On',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Received On');

                    var col_name = {
                        name : 'reserve_for_days',
                        displayName: 'Days',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Days');

                    var col_name = {
                        name : 'sent_items',
                        displayName: 'Sent Items',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Sent Items');

                    var col_name = {
                        name : 'should_be_return_on',
                        displayName: 'Return',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.con.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Return');

                }

                con.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 100,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        con.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){
                        if(row.entity.id == con.active_row_pid){
                            return 'ui-grid-active-row';
                        }
                    },
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                response.data.forEach(function (res) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        status : res.status,
                        grand_price : res.grand_price,
                        is_active : res.is_active,
                        received_items : res.received_items,
                        received_on : $filter('date')(new Date(res.received_on),'yyyy-MM-dd'),
                        reserve_for_days : res.reserve_for_days,
                        sent_items : res.sent_items,
                        should_be_return_on : $filter('date')(new Date(res.should_be_return_on),'yyyy-MM-dd')
                    };

                    con.gridOptionsComplex.data.push(temp);

                });
            }
            else{
                con.gridOptionsComplex = {
                    data : []
                };
            }

        });

    }

    function exportToCsv() {

        con.csv_array = [];
        con.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    id : row.entity.id,
                    status : row.entity.status,
                    grand_price : row.entity.grand_price,
                    is_active : row.entity.is_active,
                    received_items : row.entity.received_items,
                    received_on : row.entity.received_on,
                    reserve_for_days : row.entity.reserve_for_days,
                    sent_items : row.entity.sent_items,
                    should_be_return_on : row.entity.should_be_return_on
                };

                con.csv_array.push(temp);

            }
        });

        return con.csv_array;


    }

    function getConsignmentDetails(id) {

        con.active_row_pid = id;
        con.selected_items = [];

        con.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'vendor/consignment?access_token=' + $localStorage.am_flyrobe.access_token +
            '&id=' + id,
            headers: mainVm.headers
        }).success(function (response) {

            con.spinners     = false;
            con.show_rhs     = true;

            con.rhs_data = response.data;

        });



    }

    function selectItem(item,type) {

        if(type == 'all'){

            if(con.checked){
                con.selected_items = [];
                item.forEach(function (col) {
                    col.checked = false;
                });
            }
            else{

                var common_status = true;

                item.forEach(function (col) {
                    if( col.status != 'SOT' && col.consignment_state != item[0].consignment_state){
                        common_status = false;
                    }
                });

                if(!common_status){
                    SweetAlert.swal({
                        title: "",
                        text: "Selected items must have common state."
                    });
                    return false;
                }
                else{
                    item.forEach(function (col) {
                        if(col.status != 'SOT'){
                            con.selected_items.push(col);
                            col.checked = true;
                        }
                    });
                }
            }

            con.checked = !con.checked;

        }
        else{ //single selection
            if(item.checked){
                item.checked = false;

                var i = con.selected_items.indexOf(item);
                if (i != -1) {
                    con.selected_items.splice(i, 1);
                }

            }
            else{

                if(con.selected_items.length > 0 && item.consignment_state != con.selected_items[0].consignment_state){
                    SweetAlert.swal({
                        title: "",
                        text: "Selected items must have common state."
                    });
                    return false;
                }

                item.checked = true;

                var i = con.selected_items.indexOf(item);
                if (i == -1) {
                    con.selected_items.push(item);
                }
            }
        }//end of main else

    }

    function updateStatus(type) {

        if(con.selected_items.length <= 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            return false;
        }

        if(type == 'RCD'){
            if(con.selected_items[0].consignment_state != 'SHP'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }

            var sent_items = [];
            con.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            con.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/consignment/received',
                data : {
                    "consignment_id" : con.rhs_data.id,
                    "access_token" : $localStorage.am_flyrobe.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : false
                },
                headers: mainVm.headers
            }).success(function (response) {

                con.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    con.getConsignmentDetails(con.rhs_data.id);
                }


            });


        }
        else if(type == 'NRCD'){
            if(con.selected_items[0].consignment_state != 'SHP'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }

            var sent_items = [];
            con.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            con.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/consignment/not-received',
                data : {
                    "consignment_id" : con.rhs_data.id,
                    "access_token" : $localStorage.am_flyrobe.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : false
                },
                headers: mainVm.headers
            }).success(function (response) {

                con.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    con.getConsignmentDetails(con.rhs_data.id);
                }


            });


        }
        else{
            if(con.selected_items[0].consignment_state != 'RCD'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }


            var sent_items = [];
            con.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            con.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/item/mark-sold-out',
                data : {
                    "consignment_id" : con.rhs_data.id,
                    "access_token" : $localStorage.am_flyrobe.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : false
                },
                headers: mainVm.headers
            }).success(function (response) {

                con.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    con.getConsignmentDetails(con.rhs_data.id);
                }


            });

        }

    }

    function openModalForImage(url) {
        con.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

}


angular
    .module('flyrobe')
    .controller('consignmentCtrl', consignmentCtrl);