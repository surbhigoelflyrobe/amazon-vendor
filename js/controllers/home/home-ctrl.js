/**
 ********** Created by Surbhi Goel ************
 **/

function homeCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert, mainServices,ngDialog) {

    hm = this;

    hm.spinners     = true;


    mainVm.flyrobeState     = "Home";
    $localStorage.am_flyrobe.state = mainVm.flyrobeState;


    hm.createNewStockRequest    = createNewStockRequest;
    hm.getVendorDetails         = getVendorDetails;
    hm.openModalToCreateRequest = openModalToCreateRequest;


    hm.getVendorDetails(); //Default Call

    function getVendorDetails() {


        hm.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'vendor/get-details?access_token=' + $localStorage.am_flyrobe.access_token,
            headers: mainVm.headers
        }).success(function (response) {

            hm.spinners     = false;

            hm.send_data = {};
            hm.vendor_details = response.data;

        });


    }

    function createNewStockRequest() {

        hm.spinners     = true;

        ngDialog.close({
            template: 'newStockRequestModal',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall2 + 'vendor/new-stock-request',
            data : {
                "date_for_request": $filter('date')(new Date(),'dd/MM/yyyy'),
                "number_of_pieces": parseInt(hm.send_data.no_of_pieces),
                "special_instructions": hm.send_data.special_instr,
                "access_token" : $localStorage.am_flyrobe.access_token
            },
            headers: mainVm.headers
        }).success(function (response) {

            hm.spinners     = false;

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                });
            }else{
                SweetAlert.swal({
                    title: "",
                    text: "Created"
                });
            }



        });

    }

    function openModalToCreateRequest() {

        hm.send_data = {};

        ngDialog.open({
            template: 'newStockRequestModal',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });

    }


}


angular
    .module('flyrobe')
    .controller('homeCtrl', homeCtrl);