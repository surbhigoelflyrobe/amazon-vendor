/**
 * Created by surbhi on 07/06/17.
 */
angular
    .module('flyrobe')
    .service('mainServices', function($state) {

        var ser = this;

        ser.checkErrorStatus = function (status) {

            if(status == 401 && $state.current.name != 'login'){
                alert("Please login again");
                $state.go('login');
                return false;
            }
            /*else if (status == -1) {
             alert("Please check your internet connection.");
             return false;
             }*/

            return true;

        };

    })
    .service('commonService', function(SweetAlert,$http,ngDialog,$q,$state,mainServices,$localStorage) {

        var common = this;

        this.emptyObject = function (obj){
            for(var prop in obj) {
                if(obj.hasOwnProperty(prop))
                    return false;
            }
            return true;
        };


    });