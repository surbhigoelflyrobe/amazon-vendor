angular
    .module('flyrobe')
    .filter('camelCase', function() {
        return function(input) {
            var arr = input.split(" ");
            var output = "";
            for(var i=0; i<arr.length; i++){
                arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].substr(1).toLowerCase();
                output = output + arr[i] + " ";
            }
            return output;
        }
    });