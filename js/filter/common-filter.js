angular
    .module('flyrobe')
    .filter('itemStatus', function () {

        return function (item) {
            var list = {
                'AVL' : 'Live',
                'STV' : 'Sent To Host',
                'DMG' : 'Damaged',
                'INT' : 'in transit',
                'SOT' : 'Sold Out'
            };

            if(list[item]){
                return list[item];
            }
            else{
                return item;
            }
        };



    })
    .filter('consignmentState', function () {
        return function (item) {
            var list = {
                'SNT': 'Sent To Host',
                'RCD': 'Received',
                'NRCD': 'Not Received',
                'RTN': 'Return',
                'SOT' : 'Sold Out'
            };

            if (list[item]) {
                return list[item];
            }
            else {
                return item;
            }
        };

    });




